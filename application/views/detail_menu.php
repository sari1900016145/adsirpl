<div class="container-fluid">

    <div class="card">
        <div class="card-header">Detail Menu</h5>
        <div class="card-body">
        <div class="row">

        <?php foreach($menu as $mn): ?>
            <div class="col-md-4"> 
                <img src="<?php echo base_url().'/uploads/'.$mn->gambar?>" class="card-img-top">
            </div> 
            <div class="col-md-8"> 
                <table class="table">
                    <tr>
                        <td>Nama Menu</td>
                        <td><strong><?php echo $mn->nama_menu ?></strong></td>
                    </tr>
                    
                    <tr>
                        <td>Varian</td>
                        <td><strong><?php echo $mn->varian ?></strong></td>
                    </tr>
                    <tr>
                        <td>Stok</td>
                        <td><strong><?php echo $mn->stok ?></strong></td>
                    </tr>
                    <tr>
                        <td>Harga</td>
                        <td><strong><div class="btn btn-sm btn-success">Rp. <?php echo number_format($mn->harga, 0,',','.')?>
                    </strong></td>
                    </tr>
                </table>

                <?php echo anchor('dashboard/tambah_ke_keranjang/'.$mn->id_menu,'<div class="btn 
                    btn-sm btn-primary">Tambah ke Keranjang</div>') ?>
                <?php echo anchor('dashboard/index/','<div class="btn 
                    btn-sm btn-danger">Kembali</div>') ?>

            </div>
        </div>
    <?php endforeach ?>
        </div>
    </div>
</div>
