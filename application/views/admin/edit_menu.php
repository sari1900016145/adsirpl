<div class="container-fluid">
    <h3><i class="fas fa-edit"></i>Edit Menu</h3>
    <?php foreach($menu as $mn) : ?>
        <form method="post" action="<?php echo base_url().'admin/data_menu/update' ?>">
            <div class="for-group">
                <label>Nama Menu</label>
                <input type="text" name="nama_menu" class="form-control" value="<?php echo $mn->nama_menu ?>">  
            </div>
            <div class="for-group">
                <label>Varian</label>
                <input type="hidden" name="id_menu" class="form-control" value="<?php echo $mn->id_menu ?>">
                <input type="text" name="varian" class="form-control" value="<?php echo $mn->varian ?>">  
            </div>
            <div class="for-group">
                <label>Harga</label>
                <input type="text" name="harga" class="form-control" value="<?php echo $mn->harga ?>">  
            </div>
            <div class="for-group">
                <label>Stok</label>
                <input type="text" name="stok" class="form-control" value="<?php echo $mn->stok ?>">  
            </div>
            <button type="submit" class="btn btn-primary mt-3">Simpan</button>
        </form>
    <?php endforeach; ?>
</div>
