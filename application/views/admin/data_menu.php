<div class="container-fluid">
<button class="btn btn-sm btn-primary mb-3" data-toggle="modal"
    data-target="#tambah_menu"><i class="fas fa-plus fa-sm"></i>Tambah Menu</button>


    <table class="table table-bordered">
        <tr>
            <th>NO</th>
            <th>Nama Menu</th>
            <th>Varian</th>
            <th>Harga</th>
            <th>Stok</th>
            <th colspan="3">Aksi</th>       
        </tr>

<?php 
$no=1;
foreach($menu as $mn) : ?>

<tr>
    <td><?php echo $no++ ?></td>
    <td><?php echo $mn->nama_menu ?></td>
    <td><?php echo $mn->varian ?></td>
    <td><?php echo $mn->harga ?></td>
    <td><?php echo $mn->stok ?></td>
    
    <td><?php echo anchor('admin/data_menu/edit/'.$mn->id_menu, '<div class="btn btn-primary btn-sm"><i class="fa fa-edit"></li></div>') ?></td>
    <td onclick="javascript: return confirm('Anda yakin ingin menghapus data ini?')">
    <?php echo anchor('admin/data_menu/hapus/'.$mn->id_menu, '<div class="btn btn-danger btn-sm"><i class="fa fa-trash"></li></div>') ?></td>  
</tr>
<?php endforeach; ?>
</table>
</div>

<!-- Modal -->
<div class="modal fade" id="tambah_menu" tabindex="-1" role="dialog"
aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Input Menu</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(). 'admin/data_menu/tambah_aksi'; 
        ?>" method="post" enctype="multipart/form-data" >

        <div class="form-group">
            <label>Nama Menu</label>
            <input type="text" name="nama_menu" class="form-control">
        </div>

        <div class="form-group">
            <label>Varian</label>
            <input type="text" name="varian" class="form-control">
        </div>

        <div class="form-group">
            <label>Harga</label>
            <input type="text" name="harga" class="form-control">
        </div>

        <div class="form-group">
            <label>Stok</label>
            <input type="text" name="stok" class="form-control">
        </div>

        <div class="form-group">
            <label>Gambar Produk</label><br>
            <input type="file" name="gambar" class="form-control">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>

      </form>

    </div>
  </div>
</div>

