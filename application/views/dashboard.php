<div class="container-fuild">

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
    
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

    <div class="row text-center mt-4">

        <?php foreach ($menu as $mn) : ?>
            <div class="card ml-5 mb-3" style="width: 16rem;">
                <img src="<?php echo base_url().'/uploads/'.$mn->gambar ?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title mb-1"><?php echo $mn->nama_menu ?></h5>
                    <span class="badge badge-pill badge-dark mb-3">Rp. <?php echo number_format($mn->harga, 0,',','.')?></span>
                    <?php echo anchor('dashboard/tambah_ke_keranjang/'.$mn->id_menu,'<div class="btn 
                    btn-sm btn-danger">Tambah ke Keranjang</div>') ?>
                    <?php echo anchor('dashboard/detail/'.$mn->id_menu,'<div class="btn 
                    btn-sm btn-success">Detail</div>') ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

